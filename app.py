from flask import Flask, render_template, request, abort, jsonify
import sendgrid
from sendgrid.helpers.mail import *

app = Flask(__name__)


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/contact", methods=['POST'])
def contact():

    if not request.json:
        abort(400)

    for i in ['name', 'email', 'message']:
        if not i in request.json or request.json[i] == "":
            abort(400)

    sg = sendgrid.SendGridAPIClient(apikey="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")
    from_email = Email("kapivara@kapivara.xyz")
    to_email = Email("vizoto123@gmail.com")
    subject = "Contato Kapivara"
    content = Content("text/plain", f"{request.json['name']} \n {request.json['email']} \n {request.json['message']}")
    mail = Mail(from_email, subject, to_email, content)
    response = sg.client.mail.send.post(request_body=mail.get())

    return jsonify('', 201)


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8080, debug=True)

